package org.appinventor;
import com.google.appinventor.components.runtime.HandlesEventDispatching;
import com.google.appinventor.components.runtime.EventDispatcher;
import com.google.appinventor.components.runtime.Form;
import com.google.appinventor.components.runtime.Component;
import com.google.appinventor.components.runtime.HorizontalArrangement;
import com.google.appinventor.components.runtime.Button;
class Screen1 extends Form implements HandlesEventDispatching {
  private HorizontalArrangement HorizontaleAusrichtung1;
  private Button Wasser;
  private Button Strom;
  private Button Plastik;
  private Button Tipps;
  protected void $define() {
    this.AboutScreen("TrackMyDay");
    this.AppName("TrackMyDay");
    this.BackgroundImage("TrackMyDayIcon(2).png");
    this.Icon("TrackMyDayIcon.png");
    this.Title("TrackMyDay");
    HorizontaleAusrichtung1 = new HorizontalArrangement(this);
    HorizontaleAusrichtung1.AlignHorizontal(3);
    HorizontaleAusrichtung1.AlignVertical(2);
    HorizontaleAusrichtung1.Width(LENGTH_FILL_PARENT);
    Wasser = new Button(HorizontaleAusrichtung1);
    Wasser.BackgroundColor(0xD581B5F7);
    Wasser.FontBold(true);
    Wasser.Text("Wasser");
    Wasser.TextColor(0xFF444444);
    Strom = new Button(HorizontaleAusrichtung1);
    Strom.BackgroundColor(0xFFFFC800);
    Strom.FontBold(true);
    Strom.Text("Strom");
    Strom.TextColor(0xFF444444);
    Plastik = new Button(HorizontaleAusrichtung1);
    Plastik.BackgroundColor(0xFFA6E637);
    Plastik.FontBold(true);
    Plastik.Text("Plastik");
    Plastik.TextColor(0xFF444444);
    Tipps = new Button(HorizontaleAusrichtung1);
    Tipps.BackgroundColor(0xFFE6E5AE);
    Tipps.FontBold(true);
    Tipps.Text("Tipps");
    Tipps.TextColor(0xFF444444);
  }
  public boolean dispatchEvent(Component component, String componentName, String eventName, Object[] params){
    return false;
  }
}